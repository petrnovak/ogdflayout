#ifndef UTILITIES_KK_H
#define UTILITIES_KK_H

std::string tostr(int i) {
	std::stringstream sst;
	sst << i;
	return sst.str();
}



typedef bool (*callback_function)(string); // type for conciseness
callback_function iterateFunc; // variable to store function pointer type

void setIterateFunction(callback_function pFunc)
{
    iterateFunc = pFunc; // store
}


int iterateThroughFiles(string inputDir) {
	string dirArg = inputDir;
	if (dirArg[dirArg.size()-1] != '\\') dirArg.append("\\");
	
	// Now run through the directory and process each graph found
	List<string> files;
	List<string> subdirs;
	subdirs.pushBack(dirArg);
	
	int instances = 0;

	while (!subdirs.empty()) {
		string curdir = subdirs.popFrontRet();
		List<string> cursubdirs;
		cout << "Scanning directory "<< curdir <<"\n";
		getSubdirs(curdir.c_str(),cursubdirs);
		for (ListIterator<string> itsub = cursubdirs.begin(); itsub.valid(); itsub++)
		{
			subdirs.pushBack(curdir+(*itsub));
		}
		getFilesAppend(curdir.c_str(),files);
	
		while (!files.empty()) {
			string testfile = files.popFrontRet();
			
			cout << "\nNr "<<instances++<< " "<< testfile << "\n\n";
			iterateFunc(curdir+"\\"+testfile);
			
		}
	
	}
	return instances;
}

//file conversion

//ncol format is layout format without style and layout lines
bool ncolToGML(const char* fileName) {
	Graph G;
	bool result = readColumns(G,fileName,0,1);
	if (!result) return false;
	string str(fileName);
	(str.erase(str.find_last_of("."))).append(".gml");
	GraphIO::writeGML(G,str);
	return true;
}

bool gmlToOgml(const char* fileName){
	Graph G;
	GraphAttributes GA(G);
	bool result = GraphIO::readGML(GA,G,fileName);
	if (!result) return false;
	string str(fileName);
	(str.erase(str.find_last_of("."))).append(".ogml");
	GraphIO::writeOGML(GA,str);
	return true;
}

// Also gets some attributes
bool gml2Ogml(const char* fileName) {

	Graph G;
	
	ifstream is(fileName); 
	string s(fileName);
	s.erase(s.find_last_of(".")+1, 3);
	s += "ogml";
		
	if (!is.good())
	{
		cerr << "Could not open file "<<fileName<<"\n"; 
		return false;
	}
	GraphAttributes GA(G,GraphAttributes::nodeLabel | 
						GraphAttributes::threeD |
						GraphAttributes::nodeGraphics | 
						GraphAttributes::nodeStyle | GraphAttributes::edgeIntWeight | GraphAttributes::edgeDoubleWeight |
							GraphAttributes::edgeStyle| 
							GraphAttributes::edgeGraphics |
							GraphAttributes::edgeStyle);
	GraphIO::readGML(GA,G,is); //free
	cout << s <<": Number of nodes and edges: "<<G.numberOfNodes() << " " << G.numberOfEdges()<<"\n";
	GraphIO::writeOGML(GA, s);
}


//function to write simple graph format readable by Biolayout 3D
bool writeTabFile(GraphAttributes &GA, const Graph &G,string &fileName)
{
	ofstream os;
	os.open(fileName.c_str());
	if (!os.good())
		return false;
	edge e;
	forall_edges(e, G) {
		std::string slabel = (GA.label(e->source()).length()>0 ? GA.label(e->source()) : tostr(e->source()->index()));
		if (slabel.compare(0,1,"\"") == 0) {
			slabel.erase(0,1);
			slabel.erase(slabel.length()-1,1);
		}
		std::string tlabel = (GA.label(e->target()).length()>0 ? GA.label(e->target()) : tostr(e->target()->index()));
		if (tlabel.compare(0,1,"\"") == 0) {
			tlabel.erase(0,1);
			tlabel.erase(tlabel.length()-1,1);
		}
		os << "\""<< slabel<<"\"" << "\t\""<< tlabel<<"\""<<"\n";
	}
	node v;
	forall_nodes(v, G) {
		std::string slabel = (GA.label(v).length()>0 ? GA.label(v) : tostr(v->index()));
		if (slabel.compare(0,1,"\"") == 0) {
			slabel.erase(0,1);
			slabel.erase(slabel.length()-1,1);
		}
		os << "//NODECOORD\t\"" << slabel << "\"\t" << GA.x(v)<<"\t"<< GA.y(v)<<"\t" << 
			((GA.attributes() & GraphAttributes::threeD) ? GA.z(v) : 1.0) <<"\n";
	}
	os.close();
	return true;
}
// Convenience
bool writeTabFile(GraphAttributes &GA, string &fileName) {
	writeTabFile(GA, GA.constGraph(), fileName);
}

bool writeJson(GraphAttributes &GA, const Graph &G, string &fileName)
{
	ofstream os;
	os.open(fileName.c_str());
	if (!os.good())
		return false;

	NodeArray<int> idToIndex(G);
	os << "{\"nodes\":[";
	node v;
	int indexCount = 0;
	forall_nodes(v, G) {
		std::stringstream ss;
		ss << v->index();
		os << "{\"Id\":\""<<(GA.label(v).length() > 0 ? GA.label(v) : ss.str())<<"\",\"group\":"<<v->index()<<"}";
		idToIndex[v] = indexCount++;
		if (v != G.lastNode()) os << ",";
	}
	os << "]"; //finish node section
	os << ",\"links\":[";
	edge e;
	forall_edges(e, G) {
		os << "{\"source\":"<<idToIndex[e->source()]<<",\"target\":"<<idToIndex[e->target()]<<"}";//GA.label(e->source())
		if (e != G.lastEdge()) os << ",";
	}
	os << "]}";
	os.close();
	return true;
}
#endif