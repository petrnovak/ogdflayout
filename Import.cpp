#include "Import.h"


#include <ogdf/basic/Logger.h>
#include <ogdf/basic/HashArray.h>
#include <ogdf/fileformats/GraphIO.h>

#include <string.h>
#include <vector>

using std::string;


struct coord { int x; int y; };

// Todo: Add switch to read on to layout and style info, then have a method
// to either stop there (columns for cp files) or go on (readLayout for layout files)
//! Reads graph \a G in column format from file \a fileName, where each line defines an edge of \a G.
//! Column \a a defines start vertex, column \a b defines end vertex, remainder is saved as edge int weight.
bool readColumnsStream(Graph &G, GraphAttributes &GA, std::istream &is, unsigned int a, unsigned int b, char commentStart)
{
	// These will later be part of the interface, to be set by the user with position/type/storage tripels
	const int edgeTypeIndex = 2; // column index of edge type info, -1 is no edge type info available
	int edgeType = -1;

	G.clear();
	char buffer[SIMPLE_LOAD_BUFFER_SIZE];
	char* pch = NULL;
	char firstChar = '#';
	const char* delimiters = " \t";
	
	coord* coords = NULL;
	int nodeCount = 0;
	Array<node> indexToNode;
	HashArray<string, node> labelToNode;


	//try to read the first line to get the graph size
	while (!is.eof())
	{
		//read a line
		is.getline(buffer, SIMPLE_LOAD_BUFFER_SIZE-1);

		//---------------------
		//jump over non content
		if (strlen(buffer) == 0) continue; 
		//but: be aware of non empty lines with solely spaces below
		
		pch = strtok(buffer, delimiters);
		if (pch == NULL) continue;

		firstChar = pch[0];

		if (firstChar == commentStart) 
		{
			cout<<"Read comment line\n"; continue;
		}

		if (edgeTypeIndex == 0)
			edgeType = atoi(pch);

		for (unsigned int k = 0; k < a; k++) {
			pch = strtok(NULL, delimiters);
			if (pch == NULL) {
				cerr << "Could not read label 1\n";
				continue;
			}
			if (edgeTypeIndex == k+1)
				edgeType = atoi(pch);
		}
		string node1label(pch);
		//cout << "Read label 1 "<<node1label<<"\n";

		for (unsigned int k = a; k < b; k++)
		{
			pch = strtok(NULL, delimiters);
			if (pch == NULL) {
				cerr << "Could not read label 2\n";
				continue;
			}
			if (edgeTypeIndex == k+1)
				edgeType = atoi(pch);
		}
		string node2label(pch);
		//cout << "Read label 2 "<<node2label<<"\n";
		//get edgetype if index > b
		bool weightSet = false;
		for (unsigned int k = b; pch != NULL; k++)
		{
			pch = strtok(NULL, delimiters);
			if (pch == NULL) {
				continue;
			}
			if (edgeTypeIndex == k+1) {
				weightSet = true;
				edgeType = atoi(pch);
			}
		}
	
		if (!labelToNode.isDefined(node1label))
		{
			//cout << "Creating undefined start vertex "<<node1label<<"\n";
			node v = labelToNode[node1label] = G.newNode();
			GA.label(v) = node1label;
			nodeCount++;
		}
		if (!labelToNode.isDefined(node2label))
		{
			//cout << "Creating undefined end vertex "<<node2label<<"\n";
			node v = labelToNode[node2label] = G.newNode();
			GA.label(v) = node2label;
			nodeCount++;
		}

		OGDF_ASSERT(labelToNode.isDefined(node1label));
		OGDF_ASSERT(labelToNode.isDefined(node2label));
		edge e = G.newEdge(labelToNode[node1label], labelToNode[node2label]);

		//cout<<"Creating edges "<< node1label <<"->" << node2label<<"\n";
		if (weightSet) {
			OGDF_ASSERT(GA.attributes() & GraphAttributes::edgeIntWeight);
			GA.intWeight(e) = edgeType;
			//cout<<"With weight "<<edgeType<<"\n";
		}
		
	}//while searching for number of nodes


	if (coords != NULL) delete[] coords;

	cout <<"Read #nodes: "<<G.numberOfNodes()<<", #edges "<<G.numberOfEdges()<<"\n";

	return true;
}
//! Reads graph \a G in column format from file \a fileName, where each line defines an edge of \a G.
//! Column \a a defines start vertex, column \a b defines end vertex.
bool readColumns(Graph &G, const char *fileName, unsigned int a, unsigned int b)
{
	std::ifstream is(fileName);
	if (!is.good()) return false;
	GraphAttributes GA(G,GraphAttributes::nodeLabel | 
							GraphAttributes::nodeGraphics | 
							GraphAttributes::nodeTemplate |
							GraphAttributes::edgeType |
							GraphAttributes::nodeStyle |GraphAttributes::edgeGraphics |
							GraphAttributes::edgeStyle | 
							GraphAttributes::edgeIntWeight );
	return readColumnsStream(G, GA, is, a, b);
}
//! Reads graph \a G with attributes \a GA in column format from file \a fileName
bool readColumns(Graph &G, GraphAttributes &GA, const char *fileName, unsigned int a, unsigned int b)
{
	std::ifstream is(fileName);
	return readColumnsStream(G, GA, is, a, b);
}

#include <iostream>
#include <string>

const std::string trim(const std::string& pString,
                       const std::string& pWhitespace = " \t")
{
    const size_t beginStr = pString.find_first_not_of(pWhitespace);
    if (beginStr == std::string::npos)
    {
        // no content
        return "";
    }

    const size_t endStr = pString.find_last_not_of(pWhitespace);
    const size_t range = endStr - beginStr + 1;

    return pString.substr(beginStr, range);
}

const std::string reduce(const std::string& pString,
                         const std::string& pFill = " ",
                         const std::string& pWhitespace = " \t")
{
    // trim first
    std::string result(trim(pString, pWhitespace));

    // replace sub ranges
    size_t beginSpace = result.find_first_of(pWhitespace);
    while (beginSpace != std::string::npos)
    {
        const size_t endSpace =
                        result.find_first_not_of(pWhitespace, beginSpace);
        const size_t range = endSpace - beginSpace;

        result.replace(beginSpace, range, pFill);

        const size_t newStart = beginSpace + pFill.length();
        beginSpace = result.find_first_of(pWhitespace, newStart);
    }

    return result;
}

const char *WHITESPACE=" \t\n\r\"";

char *trimwhitespace(char *str)
{
  int spacesAtStart = strspn(str, WHITESPACE);
  char *result = str + spacesAtStart;
  int lengthOfNonSpace = strcspn(result, WHITESPACE);
  result[lengthOfNonSpace] = 0;
  return result;
}



