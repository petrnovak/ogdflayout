// FileFormatReader - Collection of some crappy graph file loaders
// you probably only need once in your life...

#ifndef IMPORT_H
#define IMPORT_H

#define SIMPLE_LOAD_BUFFER_SIZE 4096


#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/basic/GridLayout.h>
#include <ogdf/basic/HashArray.h>
#include <ogdf/basic/simple_graph_alg.h>

using namespace ogdf;


//! Reads graph \a G in column format from stream \a is, where each line defines an edge of \a G.
//! Column \a a (starting at index 0) defines start vertex, column \a b defines end vertex.
//! Precondition: a < b
bool readColumnsStream(Graph &G, GraphAttributes &GA, std::istream &is, unsigned int a = 0, unsigned int b = 1, char commentStart = '#');
//! Reads graph \a G in column format from file \a fileName
bool readColumns(Graph &G, const char *fileName, unsigned int a, unsigned int b);
//! Reads graph \a G with attributes \a GA in column format from file \a fileName
bool readColumns(Graph &G, GraphAttributes &GA, const char *fileName, unsigned int a = 0, unsigned int b = 1);
#endif