// OGDFTest.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/basic/simple_graph_alg.h>

#include <ogdf/basic/NodeArray.h>

#include <ogdf/fileformats/GraphIO.h>

#include <ogdf/energybased/FMMMLayout.h>
#include <ogdf/energybased/FastMultipoleEmbedder.h>
#include <ogdf/energybased/StressMinimization.h>
// Just in case...
//#include <ogdf/energybased/SpringEmbedderKK.h>
//#include <ogdf/energybased/SpringEmbedderFR.h>
//#include <ogdf/energybased/DavidsonHarelLayout.h>

#include <algorithm>
#include <string>
#include <sstream>
#include <math.h>

#include "Import.h"
#include "UtiPetr.h"



using namespace ogdf;



#include <algorithm>

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}

void outputSomeHelp(char* fileName) {
		cout << "Usage: "<<fileName<< " [Options] filename\n";
		cout << "where Options are \n";
		cout << "-alg A \n" << "\t Selection of the layout algorithm (A = [fmmm|sm|fme]), default sm\n";
		cout << "-out O \n" << "\t Output file name, default filename_A.format\n";
		cout << "-iff I \n" << "\t Input file format (I = [gml|layout]), default gml, layout �s assumed if input filename has extension .layout and no format given\n";
		cout << "-off O \n" << "\t Output file format (O = [gml|layout|ogml]), default gml\n";

}
bool hasLayoutExtension(char* c) {
	string s(c);
	if (s.erase(0,s.find_last_of(".")) == ".layout")
		return true;
	else return false;
}
enum aformat {fgml, flayout, fogml};    // Add further formats here
enum amethod {mfmmm, msm, mfme}; // Add further methods here 

const bool threeD = true; // Switch to compute 3d layout when using SM method
const long attributes = GraphAttributes::nodeGraphics | GraphAttributes::nodeTemplate |
		GraphAttributes::edgeGraphics | GraphAttributes::nodeStyle | GraphAttributes::edgeStyle
		| GraphAttributes::nodeLabel | GraphAttributes::edgeLabel |GraphAttributes::edgeIntWeight
		| GraphAttributes::edgeDoubleWeight | GraphAttributes::nodeWeight
		| (threeD ? GraphAttributes::threeD : 0);

//! Reads a graph from a file with format \a theFormat into structures \a G and \a GA
//! Returns true on success
bool readGraph(Graph &G, GraphAttributes &GA, aformat theFormat, char* fileName) {
	cout << "Reading file " << fileName << " assuming format " << (theFormat == fgml ? "GML" : "LAYOUT") << "\n";
	switch (theFormat) {
		case fgml:
			if (GraphIO::readGML(GA, G, fileName)) cout << "Read " << G.numberOfNodes() << " nodes, " << G.numberOfEdges() << " edges\n";
			else cerr << "Could not read file!\n";
			break;
		case flayout:
			if (readColumns(G, GA, fileName)) cout << "Read " << G.numberOfNodes() << " nodes, " << G.numberOfEdges() << " edges\n";
			else cerr << "Could not read file!\n";
			break;
		default: return false;
	}
	return true;
}

//! Runs a layout of type \a theMethod on structure \a GA
//! In case you want to change options, do that here for
//! the methods.
bool runLayout(GraphAttributes &GA, amethod theMethod) {
	double realTime;
	switch (theMethod) {
		case mfmmm: {
			FMMMLayout fmmm;
			ogdf::usedTime(realTime);
			fmmm.call(GA);
			realTime = ogdf::usedTime(realTime);
			break;
		}
		case mfme: {
			FastMultipoleMultilevelEmbedder fme;
			ogdf::usedTime(realTime);
			fme.call(GA);
			realTime = ogdf::usedTime(realTime);
			break;
		}
		case msm: {
			StressMinimization sm;
			if (threeD) 
				cout <<"Currently 3D can only be switched on by setting DIMENSION_COUNT in file PivotMDS.h to true\n";
			// Example for option setter
			// sm.convergenceCriterion(StressMinimization::STRESS);
			//sm.setEdgeCosts(40);
			//sm.setIterations(100);
			ogdf::usedTime(realTime);
			sm.call(GA);
			realTime = ogdf::usedTime(realTime);
			break;
		}
		default: return false;
	}
	cout << "Layout done. Running time "<< realTime << "\n";

	return true;
}

void writeResult(GraphAttributes &GA, std::string fileName, aformat format) {
	switch (format) {
		case fgml:	GraphIO::writeGML(GA,fileName.c_str()); break;
		case fogml:	GraphIO::writeOGML(GA,fileName.c_str()); break;
		case flayout: writeTabFile(GA, fileName); break;
	}

}

int main(int argc, char* argv[])
{
	// Help the user with some info
	if (argc == 1) {
		outputSomeHelp(argv[0]);
		return 0;
	}

	// Store our parameter settings
	char* fName;
	aformat defaultFormat = fgml;
	amethod defaultMethod = msm;
	aformat defaultOFormat = fgml;

	// Input and output file name
	fName = argv[argc-1];
	string ofName(fName);
	ofName.erase(ofName.find_last_of("."), string::npos);
	string oFormatString(".gml");
	string algString("_sm");

	if (argc > 2) {
		char* foption;

		// format
		foption = getCmdOption(argv, argv + argc, "-iff");
		if (foption != 0) {
			if (strcmp(foption, "layout") == 0)
				defaultFormat = flayout;
		} else {
			// Try to derive format from extension 
			std::string sf(fName);
			if (sf.compare(sf.find_last_of("."), 7, ".layout") == 0)
				defaultFormat = flayout;
		}
		// method
		foption = getCmdOption(argv, argv + argc, "-alg");
		if (foption != 0) {
			if (strcmp(foption, "fmmm") == 0)
			{
				defaultMethod = mfmmm;
				algString.assign("_fmmm");
			}
			else if (strcmp(foption, "fme") == 0) {
				defaultMethod = mfme;
				algString.assign("_fme");
			}
		}
		// output format
		foption = getCmdOption(argv, argv+argc, "-off");
		if (foption != 0) {
			if (strcmp(foption, "layout") == 0)
				defaultOFormat = flayout;
			else if (strcmp(foption, "ogml") == 0)
				defaultOFormat = fogml;
			//we ignore none fitting format strings here...
		}
		switch (defaultOFormat) {
			case fgml: oFormatString.assign(".gml"); break;
			case flayout: oFormatString.assign(".layout"); break;
			case fogml: oFormatString.assign(".ogml"); break;
			default: oFormatString.assign(".pleaseFixTheFormat"); break; // ok, just one joke per file
		}
		//output file name
		foption = getCmdOption(argv, argv+argc, "-out");
		if (foption != 0) {
			ofName.assign(foption);
		} else {
			ofName.append(algString+oFormatString);
		}
	} else {
		ofName.append(algString+oFormatString);
	}
	Graph G;
	GraphAttributes GAT(G, attributes);
	readGraph(G, GAT, defaultFormat, fName);
	// Adjust node sizes if needed
	//{
	 GAT.setAllWidth(10.0);
	 GAT.setAllHeight(10.0);
	//}
	runLayout(GAT, defaultMethod);
	writeResult(GAT, ofName, defaultOFormat);

	cout << "Done\n";
			
    return 0;

}

